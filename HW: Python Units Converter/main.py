'''
    REMINDER TO MERGE TO MAIN BRANCH AFTER EDITS (AND SUBMIT)

    Objective: Create a Python program that takes a number as input from the user and converts it into kilobytes (KB), kibibytes (kiB), megabytes (MB), and mebibytes (MiB).

    Instructions:

        Begin by prompting the user to enter a numerical value. Only integers are allowed (123), not floating point (1.23)

        Convert the user's input into the following units:

            Kilobytes (KB): 1 kilobyte is equal to 1000 bytes.
            Kibibytes (kiB): 1 kibibyte is equal to 1024 bytes.
            Megabytes (MB): 1 megabyte is equal to 1000 kilobytes.
            Mebibytes (MiB): 1 mebibyte is equal to 1024 kibibytes.
            
        Display the converted values along with appropriate labels to clearly indicate the unit of measurement (e.g., "X kilobytes," "Y kibibytes," etc.). You may use formatted output to make the results easy to read.

        Ensure that your program handles errors gracefully. If the user enters invalid input (e.g., non-numeric input or negative values), provide a clear error message and allow the user to try again.

        Test your program with various input values to ensure it produces correct and accurate conversions.
'''

def convert_units(bytes: int) -> None:
    print("Conversions: ")
    print(f"{bytes:,d} bytes")
    
    kilobytes = bytes / 1000
    print(str(kilobytes) + " kilobytes")
    
    kibibytes = bytes / 1024
    print(str(kibibytes) + " kibibytes")
    
    megabytes = kilobytes / 1000
    print(str(megabytes) + " megabytes")
    
    mebibytes = kibibytes / 1024
    print(str(mebibytes) + " mebibytes")
    
    print()

def main():
    while True:
        try:
            input_bytes = int(input("Please enter a number: "))
            if input_bytes < 0:
                raise ValueError()-1
            print()
            convert_units(input_bytes)
            break
        except ValueError as e:
            print("Please input a positive integer")
            print()
            
if __name__ == "__main__":
    main()